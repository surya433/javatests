package com.surya.test;

import java.util.concurrent.TimeUnit;

/**
 * Test to show liveness failure. It uses effective usage of volatile to stop
 * threading.
 * 
 * @author Surya
 *
 */
public class LivenessFailureTest {
	private static volatile boolean stopRequested = false;

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("/*********LIVENESS FAILURE TEST**********************/");
		Thread backgroundThread = new Thread(new Runnable() {
			public void run() {
				int i = 0;
				while (!stopRequested)
					i++;
				System.out.println("Thread Stopping.");
			}
		});
		System.out.println("Thread Starting.");
		backgroundThread.start();
		TimeUnit.SECONDS.sleep(1);
		stopRequested = true;
		
	}

}
