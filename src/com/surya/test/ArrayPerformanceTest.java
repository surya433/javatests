package com.surya.test;
/**
 * Test to find which way of 2D array iteration is faster?
 * @author Surya
 *
 */
public class ArrayPerformanceTest {

	public static void main(String[] args) {

		final int size = 100;
		int[][] arr = new int[100][100];
		long count = 0;
		long timer = 0;
		/*************************************************/
		count = 0;
		timer = System.nanoTime() ;
		System.out.println("Current Timer: " + timer);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				arr[i][j] = 10;
				count++;
			}
		}

		System.out.println("Count: " + count);
		getElapsed(timer);

		/*************************************************/

		count = 0;
		timer = System.nanoTime() ;
		System.out.println("Current Timer: " + timer);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				arr[j][i] = 10;
				count++;
			}
		}

		System.out.println("Count: " + count);
		getElapsed(timer);

	}

	public static long getElapsed(long prevTime) {
		long elapsed = System.nanoTime() - prevTime;
		System.out.println("Time Elapsed : " + elapsed);
		return elapsed;
	}

}
