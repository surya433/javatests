package com.surya.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MySerialClassMain {

	public static void main(String[] args) {

		MySerialClass serial = new MySerialClass(10, 29);
		ByteArrayOutputStream memoryOutputStream = new ByteArrayOutputStream();
		ObjectOutputStream serializer;
		try {
			serializer = new ObjectOutputStream(memoryOutputStream);
			serializer.writeObject(serial);
			serializer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// deserializing
		ByteArrayInputStream memoryInputStream = new ByteArrayInputStream(
				memoryOutputStream.toByteArray());
		ObjectInputStream deserializer;
		try {
			deserializer = new ObjectInputStream(
					memoryInputStream);
			MySerialClass deepCopy = (MySerialClass)deserializer.readObject();
			deepCopy.print();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
