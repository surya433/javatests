package com.surya.test;

import java.io.Serializable;

public class MySerialClass implements Serializable {

	private int x, y;

	public MySerialClass(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void print() {
		System.out.println(this.x + "-->" + this.y);
	}

	
}
