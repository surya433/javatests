package com.surya.test;

public class ReturningArrayMain {

	public static void main(String[] args) {
		final ReturningArray ra = new ReturningArray();
		ra.print();
		manipulate(ra);
		System.out.println("/*************************************************************/");
		ra.print();

	}

	public static void manipulate(ReturningArray ra) {
		int[] arr = ra.getArray();
		for (int i = 0; i < arr.length; i++) {
			arr[i] = 0;
		}
	}

}
