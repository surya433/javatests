package com.surya.test;

public class StaticTest {
	static final int arr[];
	/*
	 * the following code gets called when the class is loaded onto the memory.
	 */
	static {
		arr = new int[50];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i + 1;
		}
	}

	final int m;

	public StaticTest() {

	}

	// This is also a constructor. Observer that I haven;t added "public StaticTest() or the constructor name"
	{
		m = 10;
	}

	public static void print() {
		for (int a : arr) {
			System.out.println("Value: " + a);
		}
	}

	public static void main(String[] args) {

		print();
	}

}
