package com.surya.test;

public class ReturningArray {

	private final int array[] = new int[10];

	public ReturningArray() {
		for (int i = 0; i < array.length; i++) {
			array[i] = i + 1;
		}

	}

	public void print() {
		for (int a : array) {
			System.out.println("Value: " + a);
		}
	}

	public int[] getArray() {
		return array;
	}

	public int[] getClonedArray() {
		return array.clone();
	}

}
