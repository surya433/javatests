package com.surya.test;

/**
 * This is to show an unsafe thread. This code is not for execution. Just to
 * show the code...:)
 * 
 * @author Surya
 *
 */
public class SafetyFailure {

	public static void main(String[] args) {

	}

	/*************UNSAFE THREAD***************/
	public int serialNumber = 0;

	public int genNext() {
		return serialNumber++;
	}
	
	/*************SAFE THREAD***************/
	public int nextSerialNumber = 0;

	public synchronized int genNextSerialNumber() {
		return serialNumber++;
	}
}
