package com.surya.test;

public abstract class MyAbstractClass {
	
	/**
	 * This method is meant to be overrided. 
	 * @return
	 */
	public int[] cloneMe(){
		throw new RuntimeException("Always write your own code for cloning.") ;
	}

}
