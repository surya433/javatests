package com.generics;

/**
 * 
 * @author Surya
 *
 */
public class GenericsMain {

	/**@formatter:off
	 * ADD YOUR VIEW ON GENERICS HERE....PLUS APPEND YOU NAME BELOW
	 * 
	 * Basically I think generics is used to avoid use of  overloading
	 * 										--Shital Patil
	 * 
	 * If you want to create a class of specific type and want the same to be 
	 * used for other class types then use generics classes, that avoid lots of coding
	 * 										-- Surya
	 * 
	 * 
	 */

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		/**
		 * HOW TO CHECK?
		 * Remove the "Integer" and put some other classes. For example: Double, Float, Short etc
		 * 
		 * The idea is that, even if you change the type, the MyStack class will accept those.
		 * If you check the MyStack, I have not added specific for accepting Double, Float neither Short or Integer.
		 * I have only added a Generic type A. 
		 * @formatter:on
		 */
		MyStack<Integer> stack = new MyStack<Integer>(10);
		for (Integer i = 1; i < 11; i++) {
			stack.add(i);
		}
		stack.printValues();
	}

}
