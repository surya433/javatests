package com.generics;

public class MyStack<A> {

	A[] arr;
	int i;

	public MyStack(int size) {
		arr = (A[]) new Object[size];
		i = 0;
	}

	public void add(A value) {
		if (i < arr.length) {
			arr[i++] = value;
		} else {
			throw new RuntimeException("Array overflow..");
		}
	}
	
	
	public void printValues(){
		for(A value : arr){
			System.out.print("\t"+ value);
			
		}
	}
	
	public A pop(){
		return arr[--i] ;
	}

}
